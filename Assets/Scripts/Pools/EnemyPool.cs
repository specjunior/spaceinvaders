using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemyPool : MonoBehaviour
{
	[BoxGroup("Bindings"), SerializeField] private BulletPool bulletPool;
	[BoxGroup("Bindings"), SerializeField] private AudioSource audioSource;
	[BoxGroup("Audio"), SerializeField] private AudioClip spawnEnemySound;

	[BoxGroup("MovingUnits"), SerializeField] private int movesCount = 4;

	[BoxGroup("UnitsSettings"), SerializeField] private int enemyCount;
	[BoxGroup("UnitsSettings"), SerializeField] private int maxEnemyCountRow = 15;
	[BoxGroup("UnitsSettings"), SerializeField] private float distanceBetweenUnits = 0.5f;
	[BoxGroup("UnitsSettings"), SerializeField] private GameObject[] enemies;

	[BoxGroup("Debug"), SerializeField, ReadOnly] private int aliveUnits = 0;
	[BoxGroup("Debug"), SerializeField] private bool showDebugGizmo = false;
	[BoxGroup("Debug"), SerializeField] private List<Enemy> instantiatedEnemies = new List<Enemy>();

	private EnemiesStats enemiesStats;
	private Coroutine moveUnitsCoroutine;
	private Coroutine shootUnitsCoroutine;
	private int baseEnemyCount;

	//UNITY
	private void Start()
	{
		GameManager.me.introState.init.AddListener(DespawnEnemies);
		GameManager.me.prepareState.init.AddListener(SpawnEnemies);
		GameManager.me.gameState.init.AddListener(ActivateUnitsAnimations);
		GameManager.me.gameState.exit.AddListener(StopMovingShootingUnits);
		GameManager.me.endState.exit.AddListener(DespawnEnemies);
		baseEnemyCount = enemyCount;
	}

	//PUBLIC
	public void ApplyLoadedStats(EnemiesStats enemiesStats_)
	{
		enemiesStats = enemiesStats_;
	}

	public void SpawnEnemies()
	{
		enemyCount = baseEnemyCount + maxEnemyCountRow * (int)GameManager.me.Difficulty;
		StartCoroutine(SpawnUnits());
	}

	public void DespawnEnemies()
	{
		foreach(Enemy enemy in instantiatedEnemies)
		{
			Destroy(enemy.gameObject);
		}
		instantiatedEnemies.Clear();
	}

	public void DestroyUnit(Enemy unit)
	{
		aliveUnits -= 1;
		unit.gameObject.SetActive(false);

		if(aliveUnits <= 0) GameManager.me.Win();
	}

	//PRIVATE
	private void SpawnUnit(int index, int rowNumber)
	{
		float distancesBetweenUnits = (index - rowNumber * maxEnemyCountRow) * distanceBetweenUnits;

		Enemy enemy = Instantiate(enemies[rowNumber % enemies.Length], transform).GetComponent<Enemy>();
		enemy.OnStart(
			bulletPool,
			this,
			enemiesStats != null ? enemiesStats.GetDamageForUnitType(enemy.EnemyType) : -1
		);

		enemy.transform.position =
			transform.position +
			Vector3.right * (index - rowNumber * maxEnemyCountRow + distancesBetweenUnits + distanceBetweenUnits) +
			Vector3.down * rowNumber;

		audioSource.PlayOneShot(spawnEnemySound);
		instantiatedEnemies.Add(enemy);
	}

	private void ActivateUnitsAnimations()
	{
		for(int i = 0; i < instantiatedEnemies.Count; i++)
		{
			instantiatedEnemies[i].StartAnimation();
		}
	}

	private void StartMovingShootingUnits()
	{
		moveUnitsCoroutine = StartCoroutine(MoveUnits());
		shootUnitsCoroutine = StartCoroutine(ShootUnits());
	}

	private void StopMovingShootingUnits()
	{
		StopCoroutine(moveUnitsCoroutine);
		StopCoroutine(shootUnitsCoroutine);
	}

	//COROUTINES
	private IEnumerator SpawnUnits()
	{
		yield return new WaitForSeconds(0.4f);
		aliveUnits = enemyCount;

		for(int i = 0, row = 0; i < enemyCount; i++, row = i / maxEnemyCountRow)
		{
			SpawnUnit(i, row);
			yield return new WaitForSeconds(0.05f);
		}
		GameManager.me.ChangeState(GameState.game);
		StartMovingShootingUnits();
		yield return null;
	}

	private IEnumerator MoveUnits()
	{
		float direction = 1f;
		int moveCounter = movesCount / 2;
		while(true)
		{
			if(moveCounter % movesCount == 0)
				direction *= -1f;

			for(int i = 0; i < instantiatedEnemies.Count; i++)
			{
				if(instantiatedEnemies[i] == null || !instantiatedEnemies[i].gameObject.activeInHierarchy) continue;

				instantiatedEnemies[i].transform.position += Vector3.right * 0.5f * direction;
				yield return new WaitForSeconds(0.005f);
			}

			moveCounter += 1;
			yield return new WaitForSeconds(0.5f);
		}
	}

	private IEnumerator ShootUnits()
	{
		float time = 1.0f;
		System.Random rnd = new System.Random();
		float difficulty = (float)GameManager.me.Difficulty;

		while(true)
		{
			yield return new WaitForSeconds(time);
			Enemy randomUnit = instantiatedEnemies.Where(x => x.gameObject.activeInHierarchy).OrderBy(item => rnd.Next()).FirstOrDefault();
			randomUnit.Shoot();
			time = Random.Range(0.5f, 1.2f) * (10f - difficulty) / 10f;
		}
	}

	private void OnDrawGizmosSelected()
	{
		if(!showDebugGizmo) return;

		Gizmos.color = Color.yellow;
		Vector3 position;
		float distancesBetweenUnits;

		for(int i = 0, row = 0; i < enemyCount; i++, row = i / maxEnemyCountRow)
		{
			distancesBetweenUnits = (i - row * maxEnemyCountRow) * distanceBetweenUnits;
			position = transform.position;
			position +=
				Vector3.right * (i - row * maxEnemyCountRow + distancesBetweenUnits + distanceBetweenUnits) +
				Vector3.down * row;
			Gizmos.DrawCube(position, Vector3.one);
		}
	}
}