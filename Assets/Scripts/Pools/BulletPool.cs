using Sirenix.OdinInspector;
using System.Collections.Generic;
using UnityEngine;

public class BulletPool : MonoBehaviour
{
	[BoxGroup("Prefabs"), SerializeField] private GameObject enemyBullet;
	[BoxGroup("Prefabs"), SerializeField] private GameObject playerBullet;

	[BoxGroup("Settings"), SerializeField] private int enemyBulletsCapacity = 8;
	[BoxGroup("Settings"), SerializeField] private int playerBulletsCapacity = 4;

	private Stack<Bullet> enemyBullets = new Stack<Bullet>();
	private Stack<Bullet> playerBullets = new Stack<Bullet>();

	private List<Bullet> usedBullets = new List<Bullet>();
	private int availableEnemyBullets = 0;
	private int availablePlayerBullets = 0;

	private void Start()
	{
		GenerateMagazines();
		GameManager.me.prepareState.init.AddListener(ReturnAllBullets);
	}

	public void GenerateMagazines()
	{
		for(int i = 0; i < enemyBulletsCapacity; i++)
		{
			Bullet bullet = Instantiate(enemyBullet, transform).GetComponent<Bullet>();
			bullet.CreateBullet(this);
			enemyBullets.Push(bullet);
		}

		for(int i = 0; i < playerBulletsCapacity; i++)
		{
			Bullet bullet = Instantiate(playerBullet, transform).GetComponent<Bullet>();
			bullet.CreateBullet(this);
			playerBullets.Push(bullet);
		}
		availableEnemyBullets = enemyBulletsCapacity;
		availablePlayerBullets = playerBulletsCapacity;
	}

	public void GetEnemyBullet(Vector3 startPoint, float dmg)
	{
		if(availableEnemyBullets <= 0) return;
		Bullet temp = enemyBullets.Pop();
		temp.Activate(startPoint, dmg);
		usedBullets.Add(temp);
		availableEnemyBullets -= 1;
	}

	public void GetPlayerBullet(Vector3 startPoint)
	{
		if(availablePlayerBullets <= 0) return;
		Bullet temp = playerBullets.Pop();
		temp.Activate(startPoint);
		usedBullets.Add(temp);
		availablePlayerBullets -= 1;
	}

	public void ReturnBullet(Bullet bullet_)
	{
		bullet_.Deactivate();
		usedBullets.Remove(bullet_);

		if(bullet_.tag.Equals("EnemyBullet"))
		{
			bullet_.transform.position = transform.position;
			if(enemyBullets.Contains(bullet_)) return;
			enemyBullets.Push(bullet_);
			availableEnemyBullets += 1;
		}
		else if(bullet_.tag.Equals("PlayerBullet"))
		{
			bullet_.transform.position = transform.position;
			if(playerBullets.Contains(bullet_)) return;
			playerBullets.Push(bullet_);
			availablePlayerBullets += 1;
		}
	}

	private void ReturnAllBullets()
	{
		for(int i = 0; i < usedBullets.Count; i++)
		{
			ReturnBullet(usedBullets[i]);
		}
	}
}