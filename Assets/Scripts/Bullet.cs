using Sirenix.OdinInspector;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float DamageAmount => damageAmount;

	[Tooltip("If false, bullet will move straight up.")]
	[BoxGroup("Settings"), SerializeField] private bool directionDown = false;
	[BoxGroup("Settings"), SerializeField] private float speed = 10f;
	[BoxGroup("Settings"), SerializeField] private float damageAmount = 10f;
	[BoxGroup("Settings"), SerializeField] private string tagToCollideWith = "";
	private BulletPool pool;

	public void CreateBullet(BulletPool pool_)
	{
		pool = pool_;
		GameManager.me.gameState.exit.AddListener(() => pool.ReturnBullet(this));
	}

	public void Activate(Vector3 position, float dmg = 5f)
	{
		transform.position = position;
		damageAmount = dmg;
		GameManager.me.gameState.update.AddListener(UpdateBullet);
	}

	public void Deactivate()
	{
		GameManager.me.gameState.update.RemoveListener(UpdateBullet);
	}

	private void UpdateBullet()
	{
		transform.position += speed * (directionDown ? -1f : 1f) * Time.deltaTime * Vector3.up;
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag.Equals(tagToCollideWith) || collision.tag.Equals("Barrier"))
		{
			pool.ReturnBullet(this);
		}
	}
}