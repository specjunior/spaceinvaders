using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIAudio : MonoBehaviour
{
	[BoxGroup("Bindings"), SerializeField] private AudioSource audioSource;
	[BoxGroup("Bindings"), SerializeField] private AudioClip clickUI;

	public void PlayClickSound()
	{
		audioSource.PlayOneShot(clickUI);
	}
}