using Sirenix.OdinInspector;
using UnityEngine;

public class Enemy : MonoBehaviour
{
	public EnemyType EnemyType => enemyType;

	[BoxGroup("Bindings"), SerializeField] private Transform shootPoint;
	[BoxGroup("Bindings"), SerializeField] private Animator animator;
	[BoxGroup("Bindings"), SerializeField] private BulletPool bulletPool;
	[BoxGroup("Bindings"), SerializeField] private EnemyPool enemyPool;
	[BoxGroup("Bindings"), SerializeField] private AudioSource audioSource;

	[BoxGroup("Audio"), SerializeField] private AudioClip shootSound;

	[BoxGroup("Settings"), SerializeField] private EnemyType enemyType = EnemyType.Type1;
	private float damage = 0f;

	public void OnStart(BulletPool bulletPool_, EnemyPool enemyPool_, float damage_ = -1)
	{
		bulletPool = bulletPool_;
		enemyPool = enemyPool_;
		damage = damage_ < 0 ? (int)enemyType : damage_;
	}

	public void StartAnimation() => animator.SetTrigger("start");

	public void StopAnimation() => animator.SetTrigger("pause");

	public void Shoot()
	{
		audioSource.PlayOneShot(shootSound);
		bulletPool.GetEnemyBullet(shootPoint.position, (int)enemyType);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag.Equals("PlayerBullet"))
		{
			//animator.SetTrigger("hit");
			enemyPool.DestroyUnit(this);
			GameManager.me.AddDestroyedUnitsStats(enemyType);
		}
	}
}