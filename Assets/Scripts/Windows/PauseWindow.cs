using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Sirenix.OdinInspector;

public class PauseWindow : GameWindow
{
	[BoxGroup("Bindings"), SerializeField] private Button resume;
	[BoxGroup("Bindings"), SerializeField] private Button mainMenu;

	private void Start()
	{
		resume.onClick.AddListener(Resume);
		resume.onClick.AddListener(uiAudio.PlayClickSound);
		mainMenu.onClick.AddListener(MainMenu);
		mainMenu.onClick.AddListener(uiAudio.PlayClickSound);
	}

	public override void ShowWindow()
	{
		EventSystem.current.SetSelectedGameObject(resume.gameObject);
		base.ShowWindow();
	}

	private void Resume()
	{
		GameManager.me.OnPause();
	}

	private void MainMenu()
	{
		GameManager.me.ChangeState(GameState.intro);
	}
}