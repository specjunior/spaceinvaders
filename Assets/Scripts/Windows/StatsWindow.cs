using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.UI;

public class StatsWindow : GameWindow
{
	[BoxGroup("Enemies"), SerializeField] private Text type1StatsText;
	[BoxGroup("Enemies"), SerializeField] private Text type2StatsText;
	[BoxGroup("Enemies"), SerializeField] private Text type3StatsText;
	[BoxGroup("Players"), SerializeField] private Text player1HpText;
	[BoxGroup("Players"), SerializeField] private Text player2HpText;
	[BoxGroup("Players"), SerializeField] private Text difficultyText;

	private int type1Stats = 0;
	private int type2Stats = 0;
	private int type3Stats = 0;

	private void Awake()
	{
		GameManager.me.introState.init.AddListener(HideWindow);
		GameManager.me.introState.init.AddListener(ResetStats);
		GameManager.me.prepareState.init.AddListener(ShowWindow);
	}

	public void AddStats(EnemyType enemyType)
	{
		switch(enemyType)
		{
			case EnemyType.Type1:
				type1Stats += 1;
				break;
			case EnemyType.Type2:
				type2Stats += 1;
				break;
			case EnemyType.Type3:
				type3Stats += 1;
				break;
		}
		UpdateStats();
	}

	public override void ShowWindow()
	{
		player2HpText.gameObject.SetActive(GameManager.me.PlayersNumber > 1);
		base.ShowWindow();
	}

	public void UpdatePlayersHpUi(int playerNumber, float amount)
	{
		switch(playerNumber)
		{
			case 1:
				player1HpText.text = amount > 0 ? $"HP {amount}%" : "DIED!";
				break;
			case 2:
				player2HpText.text = amount > 0 ? $"HP {amount}%" : "DIED!";
				break;
		}
	}

	public void UpdateDifficulty(Difficulty difficulty)
	{
		difficultyText.text = difficulty.ToString().ToUpper();
	}

	private void ResetStats()
	{
		type1Stats = type2Stats = type3Stats = 0;
		UpdateStats();
	}

	private void UpdateStats()
	{
		type1StatsText.text = $"X {type1Stats}";
		type2StatsText.text = $"X {type2Stats}";
		type3StatsText.text = $"X {type3Stats}";
	}
}