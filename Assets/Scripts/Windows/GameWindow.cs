using Sirenix.OdinInspector;
using UnityEngine;

public class GameWindow : MonoBehaviour
{
	[BoxGroup("Bindings"), SerializeField] private CanvasGroup canvasGroup;
	[BoxGroup("Bindings"), SerializeField] protected UIAudio uiAudio;

	public virtual void ShowWindow()
	{
		canvasGroup.Enable();
	}

	public virtual void HideWindow()
	{
		canvasGroup.Disable();
	}
}