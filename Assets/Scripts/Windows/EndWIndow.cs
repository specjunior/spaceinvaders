using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class EndWIndow : GameWindow
{
	[BoxGroup("Bindings"), SerializeField] private Button resetButton;
	[BoxGroup("Bindings"), SerializeField] private Button menuButton;

	private void Start()
	{
		GameManager.me.endState.init.AddListener(ShowWindow);
		GameManager.me.endState.exit.AddListener(HideWindow);
		resetButton.onClick.AddListener(ResetGame);
		resetButton.onClick.AddListener(uiAudio.PlayClickSound);
		menuButton.onClick.AddListener(OpenMenu);
		menuButton.onClick.AddListener(uiAudio.PlayClickSound);
	}

	public override void ShowWindow()
	{
		EventSystem.current.SetSelectedGameObject(resetButton.gameObject);
		base.ShowWindow();
	}

	private void ResetGame()
	{
		GameManager.me.ChangeState(GameState.prepare);
	}

	private void OpenMenu()
	{
		GameManager.me.ChangeState(GameState.intro);
	}
}