using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MainMenu : GameWindow
{
	[BoxGroup("Bindings"), SerializeField] private GameManager gameManager;
	[BoxGroup("Bindings"), SerializeField] private Button onePlayer;
	[BoxGroup("Bindings"), SerializeField] private Button twoPlayers;

	private void Awake()
	{
		gameManager.introState.init.AddListener(ShowWindow);
		gameManager.introState.exit.AddListener(HideWindow);

		//czm delegate a nie ()=>
		onePlayer.onClick.AddListener(delegate { gameManager.PlayersNumber = 1; });
		onePlayer.onClick.AddListener(delegate { gameManager.ChangeState(GameState.prepare); });
		onePlayer.onClick.AddListener(uiAudio.PlayClickSound);

		twoPlayers.onClick.AddListener(delegate { gameManager.PlayersNumber = 2; });
		twoPlayers.onClick.AddListener(delegate { gameManager.ChangeState(GameState.prepare); });
		twoPlayers.onClick.AddListener(uiAudio.PlayClickSound);
	}

	public void StartGame()
	{
		GameManager.me.ChangeState(GameState.prepare);
	}

	public override void ShowWindow()
	{
		base.ShowWindow();
		EventSystem.current.SetSelectedGameObject(onePlayer.gameObject);
	}
}