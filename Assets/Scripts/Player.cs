using Sirenix.OdinInspector;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Player : MonoBehaviour
{
	public float Life => life;

	[BoxGroup("Bindings"), SerializeField] private Transform shootPoint;
	[BoxGroup("Bindings"), SerializeField] private Transform startPosition;
	[BoxGroup("Bindings"), SerializeField] private ContactFilter2D moveFilterToBlock;
	[BoxGroup("Bindings"), SerializeField] private Rigidbody2D rigidbody;
	[BoxGroup("Bindings"), SerializeField] private BulletPool bulletPool;
	[BoxGroup("Bindings"), SerializeField] private SpriteRenderer spriteRenderer;
	[BoxGroup("Bindings"), SerializeField] private AudioSource audioSource;

	[BoxGroup("Audio"), SerializeField] private AudioClip shootSound;
	[BoxGroup("Audio"), SerializeField] private AudioClip hurtSound;
	[BoxGroup("Audio"), SerializeField] private AudioClip deadSound;

	[BoxGroup("Settings"), SerializeField, Range(1, 2)] private int playerNumber = 1;
	[BoxGroup("Settings"), SerializeField, Range(1, 20)] private float speed = 5f;
	[BoxGroup("Settings"), SerializeField, Range(0.01f, 1.0f)] private float shootDelay = 0.3f;
	[BoxGroup("Settings"), SerializeField] private float life = 100f;

	private float lastShootTime = 0;
	private float startPlayerLife;
	private List<RaycastHit2D> moveHits = new List<RaycastHit2D>();
	private Controls controls;
	private Action<InputAction.CallbackContext> Shoots;

	private void Awake()
	{
		Shoots = (ctx) => Shoot();
		GameManager.me.gameState.init.AddListener(ActivateShoot);
		GameManager.me.gameState.exit.AddListener(Deactive);
	}

	private void Start()
	{
		startPlayerLife = life;
		controls = new Controls();

		spriteRenderer.enabled = false;
	}

	public void Move()
	{
		float direction;
		if(playerNumber == 1)
			direction = controls.Player1.Move.ReadValue<float>();
		else
			direction = controls.Player2.Move.ReadValue<float>();

		if(rigidbody.Cast(direction * Vector3.right, moveFilterToBlock, moveHits, 0.5f) <= 0)
			transform.position += direction * speed * Time.deltaTime * Vector3.right;
	}

	public void SetActive(bool activeState)
	{
		if(activeState)
			Activate();
		else
			Deactive();
	}

	private void ActivateShoot()
	{
		if(playerNumber == 1)
			controls.Player1.Shoot.performed += Shoots;//shoot => Shoot();
		else
			controls.Player2.Shoot.performed += Shoots;//shoot => Shoot();
	}

	private void Activate()
	{
		life = startPlayerLife;
		GameManager.me.UpdateHpUi(playerNumber, life);

		gameObject.SetActive(true);
		spriteRenderer.enabled = true;
		transform.position = startPosition.position;

		GameManager.me.gameState.update.AddListener(Move);
		controls.Enable();
	}

	private void Deactive()
	{
		gameObject.SetActive(false);
		if(playerNumber == 1)
			controls.Player1.Shoot.performed -= Shoots;//shoot => Shoot();
		else
			controls.Player2.Shoot.performed -= Shoots;//shoot => Shoot();
		GameManager.me.gameState.update.RemoveListener(Move);
		controls.Disable();
	}

	private void Die()
	{
		GameManager.me.PlayerDie();
		audioSource.PlayOneShot(deadSound);
		Deactive();
	}

	private void Shoot()
	{
		if(Time.time - lastShootTime > shootDelay)
		{
			bulletPool.GetPlayerBullet(shootPoint.position);
			lastShootTime = Time.time;
			audioSource.PlayOneShot(shootSound);
		}
	}

	private void Damage(float amount)
	{
		life -= amount;
		GameManager.me.UpdateHpUi(playerNumber, life);
		if(life <= 0)
			Die();
		else
			audioSource.PlayOneShot(hurtSound);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if(collision.tag.Equals("EnemyBullet"))
			Damage(collision.gameObject.GetComponent<Bullet>().DamageAmount);
	}
}