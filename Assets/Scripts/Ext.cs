using UnityEngine;

public static class Ext
{
	public static float Clamp01(this float value)
		=> value < 0 ? 0 : (value > 1 ? 1 : value);

	public static float Proc2Db(float value)
	{
		value = value.Clamp01();
		if(value <= 0)
			return -80;
		return 20.0f * Mathf.Log10(value);
	}

	public static float Db2Proc(float dB)
		=> Mathf.Pow(10.0f, dB / 20.0f);

	public static void Disable(this CanvasGroup cg)
	{
		cg.alpha = 0;
		cg.blocksRaycasts = false;
		cg.interactable = false;
	}

	public static void Enable(this CanvasGroup cg)
	{
		cg.alpha = 1;
		cg.blocksRaycasts = true;
		cg.interactable = true;
	}
}