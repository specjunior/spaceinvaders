using UnityEngine.Events;

public class State
{
	public UnityEvent init = new UnityEvent();
	public UnityEvent update = new UnityEvent();
	public UnityEvent exit = new UnityEvent();

	public void InitInvoke()
	{
		init.Invoke();
	}

	public void UpdateInvoke()
	{
		update.Invoke();
	}

	public void ExitInvoke()
	{
		exit.Invoke();
	}
}
