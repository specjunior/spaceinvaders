using Sirenix.OdinInspector;
using System.IO;
using UnityEngine;

public class GameManager : MonoBehaviour
{
	public static GameManager me { get; private set; }

	public State CurrentState
	{
		get { return currentState; }
		private set
		{
			if(value == null)
			{
				Debug.LogError("NULL");
				return;
			}
			currentState?.ExitInvoke();
			currentState = value;
			currentState.InitInvoke();
		}
	}

	public Difficulty Difficulty => difficulty;

	public int PlayersNumber
	{
		get { return playersNumber; }
		set { playersNumber = value; }
	}

	[BoxGroup("Bindings"), SerializeField] private Player player1;
	[BoxGroup("Bindings"), SerializeField] private Player player2;
	[BoxGroup("Bindings"), SerializeField] private BulletPool bulletPool;
	[BoxGroup("Bindings"), SerializeField] private EnemyPool enemyPool;
	[BoxGroup("Bindings"), SerializeField] private PauseWindow pauseWindow;
	[BoxGroup("Bindings"), SerializeField] private StatsWindow statsWindow;
	[BoxGroup("Bindings"), SerializeField] private GameWindow hintsWindow;

	[BoxGroup("Debug"), SerializeField, ReadOnly] private Difficulty difficulty = Difficulty.Easy;
	[BoxGroup("Debug"), SerializeField, ReadOnly] private int playersNumber = 1;
	[BoxGroup("Debug"), SerializeField, ReadOnly] private int alivePlayersCount = 1;
	[BoxGroup("Debug"), SerializeField, ReadOnly] private bool paused = false;

	private State currentState;
	public State introState;
	public State prepareState;
	public State gameState;
	public State winState;
	public State endState;

	//UNITY
	private void Awake()
	{
		if(me == null)
			me = this;

		introState = new State();
		prepareState = new State();
		gameState = new State();
		endState = new State();
	}

	private void Start()
	{
		LoadStats();

		prepareState.init.AddListener(EnableActivePlayers);
		prepareState.init.AddListener(delegate { statsWindow.UpdateDifficulty(difficulty); });

		prepareState.init.AddListener(hintsWindow.ShowWindow);
		prepareState.init.AddListener(delegate { prepareState.init.RemoveListener(hintsWindow.ShowWindow); });
		prepareState.exit.AddListener(hintsWindow.HideWindow);
		prepareState.exit.AddListener(delegate { prepareState.exit.RemoveListener(hintsWindow.HideWindow); });

		gameState.exit.AddListener(UnPause);

		CurrentState = introState;
	}

	private void Update()
	{
		currentState.UpdateInvoke();
	}

	//PUBLIC
	public void ChangeState(GameState newState)
	{
		switch(newState)
		{
			case GameState.intro:
				CurrentState = introState;
				break;
			case GameState.prepare:
				CurrentState = prepareState;
				break;
			case GameState.game:
				CurrentState = gameState;
				break;
			case GameState.end:
				CurrentState = endState;
				break;
		}
	}

	public void Win()
	{
		IncreaseDifficulty();
		ChangeState(GameState.end);
	}

	public void IncreaseDifficulty()
	{
		if(difficulty < Difficulty.Nightmare)
			difficulty += 1;
	}

	public void AddDestroyedUnitsStats(EnemyType enemyType)
	{
		statsWindow.AddStats(enemyType);
	}

	public void UpdateHpUi(int playerNumber, float amount)
	{
		statsWindow.UpdatePlayersHpUi(playerNumber, amount);
	}

	public void PlayerDie()
	{
		alivePlayersCount -= 1;
		if(alivePlayersCount <= 0)
			CurrentState = endState;
	}

	public void OnPause()
	{
		if(currentState == gameState)
			if(paused)
				UnPause();
			else
				Pause();
	}

	[Button]
	public void Save()
	{
		string convertedToJson = JsonUtility.ToJson(new EnemiesStats(), true);
		string path = $"{Path.GetDirectoryName(Application.dataPath) }/enemies.space";
		File.WriteAllText(path, convertedToJson);
	}

	public void LoadStats()
	{
		string path = $"{Path.GetDirectoryName(Application.dataPath)}/enemies.space";
		if(File.Exists(path))
		{
			string data = File.ReadAllText(path);

			try
			{
				EnemiesStats loadedData = JsonUtility.FromJson<EnemiesStats>(data);
				FixLoadedSave(loadedData);

				enemyPool.ApplyLoadedStats(loadedData);
				difficulty = loadedData.difficulty;
			}
			catch
			{
				Save();
				LoadStats();
				Debug.LogError("[GameManager] Cannot load save", gameObject);
			}
		}
		else
		{
			Save();
			LoadStats();
		}
	}

	//PRIVATE
	private void Pause()
	{
		paused = true;
		pauseWindow.ShowWindow();
		Time.timeScale = 0;
	}

	private void UnPause()
	{
		paused = false;
		pauseWindow.HideWindow();
		Time.timeScale = 1;
	}

	private void FixLoadedSave(EnemiesStats loadedData)
	{
		loadedData.enemy1Damage = loadedData.enemy1Damage < 0 ? 0 : loadedData.enemy1Damage;
		loadedData.enemy2Damage = loadedData.enemy2Damage < 0 ? 0 : loadedData.enemy2Damage;
		loadedData.enemy3Damage = loadedData.enemy3Damage < 0 ? 0 : loadedData.enemy3Damage;

		loadedData.difficulty = loadedData.difficulty > Difficulty.Nightmare ? Difficulty.Nightmare : loadedData.difficulty;
		loadedData.difficulty = loadedData.difficulty < Difficulty.Easy ? Difficulty.Easy : loadedData.difficulty;
	}

	private void EnableActivePlayers()
	{
		player1.SetActive(true);
		player2.SetActive(playersNumber > 1);
		alivePlayersCount = playersNumber;
	}
}