using UnityEngine;
using UnityEngine.EventSystems;

public class SelectOnHighlight : MonoBehaviour, IPointerEnterHandler
{
	public void OnPointerEnter(PointerEventData eventData)
	{
		EventSystem.current.SetSelectedGameObject(gameObject);
	}
}
