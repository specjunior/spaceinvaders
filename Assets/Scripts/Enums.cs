public enum GameState
{
	intro = 0,
	prepare = 1,
	game = 2,
	pause = 3,
	end = 4
}

public enum EnemyType
{
	//damages
	Type1 = 5,
	Type2 = 10,
	Type3 = 15
}

public enum Difficulty
{
	//damages
	Easy = 0,
	Normal = 1,
	Hard = 2,
	Nightmare = 3
}