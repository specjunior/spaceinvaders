public class EnemiesStats
{
	public float enemy1Damage = 5.0f;
	public float enemy2Damage = 10.0f;
	public float enemy3Damage = 15.0f;

	public Difficulty difficulty = Difficulty.Easy;

	public float GetDamageForUnitType(EnemyType enemyType)
	{
		switch(enemyType)
		{
			case EnemyType.Type1:
				return enemy1Damage;
			case EnemyType.Type2:
				return enemy2Damage;
			case EnemyType.Type3:
				return enemy3Damage;
			default:
				return -1;
		}
	}
}